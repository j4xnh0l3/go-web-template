package models

import (
	"testing"
)

func TestExampleValidate(t *testing.T) {
	e := Example{}
	isValid := e.Validate()
	if isValid {
		t.Errorf("%v should not be valid", e)
	}
}
