Name:           myexe
# defined with rpmbuild --define
Version:        %{version}
# defined with rpmbuild --define
Release:        %{release}
Summary:        TODO
License:        TODO
URL:            TODO

BuildRequires:  gcc
BuildRequires:  golang >= 1.12

Requires(pre): /usr/sbin/useradd, /usr/sbin/groupadd, /usr/bin/getent
Requires(postun): /usr/sbin/userdel

%description
# include your full description of the application here.
TODO

%prep
#%setup -q -n myexe-%{version}
cd %{_builddir}/myexe-%{version}

%build
cd %{_builddir}/myexe-%{version}
make

%install
cd %{_builddir}/myexe-%{version}
install -d %{buildroot}/%{_bindir}
make install DESTDIR=%{buildroot} PREFIX=/usr LOGDIR=%{buildroot}/var/log/myexe
#install -p -m 0755 ./myexe %{buildroot}%{_bindir}/myexe

%post
getent group myexe 1>/dev/null 2>&1 || \
	groupadd -r -g 1234 myexe 1>/dev/null 2>&1
getent passwd myexe 1>/dev/null 2>&1 || \
	useradd -r -u 1235 -g 1234  \
	-d /path/to/program -s /sbin/nologin myexe \
	1>/dev/null 2>&1

%postun
/usr/sbin/userdel myexe
/usr/sbin/groupdel myexe

%files
%defattr(-,root,root,-)
#%doc LICENSE README.md
%{_bindir}/myexe-server
%config /etc/logrotate.d/myexe
%config /etc/myexe/prod-config.json
%{_usr}/lib/systemd/system/myexe.service
