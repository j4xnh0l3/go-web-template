# Example invocation:
# docker build -t myexe .
# docker run --net="host" localhost/myexe

ARG BUILD_IMAGE="docker.io/library/golang:1.14-alpine"
# You can override this with an image that contains more debugging tools,
# etc..
ARG PROD_IMAGE="scratch"

FROM "${BUILD_IMAGE}" as builder
WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 go build -o myexe-server

FROM "${PROD_IMAGE}"
WORKDIR /app
COPY --from=builder /app/myexe-server /usr/bin/
COPY ./files ./files
EXPOSE 8080
CMD ["myexe-server", "-config", "./files/prod-config.json"]
