module gitlab.com/jaelen/go-web-template

go 1.12

require (
	github.com/prometheus/client_golang v1.1.0
	github.com/sirupsen/logrus v1.2.0
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce
)
