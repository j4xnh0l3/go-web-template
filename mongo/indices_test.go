package mongo

import (
	"testing"

	"gitlab.com/jaelen/go-web-template/config"
)

func TestEnsureIndices(t *testing.T) {
	cfg := config.LoadConfig("../test_data/test_config.json")
	config.Cfg = &cfg
	EnsureIndices()
}
