package mongo

import (
	"gopkg.in/mgo.v2"

	"gitlab.com/jaelen/go-web-template/config"
	"gitlab.com/jaelen/go-web-template/util"
)

// You must call "defer session.Close()" on the result
func Session() *mgo.Session {
	session, err := mgo.Dial(config.Cfg.MongoAddress)
	util.FatalError(err)
	return session
}
