#!/bin/sh -x

DBPATH=/tmp/myexe_test_dbpath
rm -rf $DBPATH
sleep 1.0
mkdir $DBPATH

mongod --port 9900 --dbpath "$DBPATH" 1>/dev/null &
pid=$!

# Run the tests
go test -cover -coverprofile=coverage.out -covermode=atomic ./... \
&& \
go tool cover -html=coverage.out

kill $pid
