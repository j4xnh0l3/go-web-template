package util

import (
	"errors"
	"testing"

	log "github.com/sirupsen/logrus"
)

func TestFatalError(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered from panic in TestFatalError")
		} else {
			t.Error("TestFatalError did not panic")
		}
	}()
	err := errors.New("Fatal error")
	FatalError(err)
}
