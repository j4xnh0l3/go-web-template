package util

import (
	"bytes"
	"errors"
	"net/http"
	"testing"
)

type errorReader struct {
}

func (e errorReader) Read(
	p []byte,
) (
	n int,
	err error,
) {
	return 0, errors.New("I error every time")
}

type V struct {
	A int32
}

func TestHttpClientFactory(t *testing.T) {
	HttpClientFactory()
}

func TestReadJSONBody(t *testing.T) {
	v := V{}
	req, err := http.NewRequest(
		"GET",
		"localhost/index.html",
		bytes.NewReader([]byte(`{"A": 123}`)),
	)
	if err != nil {
		t.Fatalf("ReadJSONBody: http.NewRequest failed: '%v'\n", err)
	}
	err = ReadJSONBody(req, &v)
	if err != nil {
		t.Errorf("ReadJSONBody returned an error: '%v'\n", err)
	}
	if v.A != 123 {
		t.Errorf(
			"ReadJSONBody: expected v.A == 123, got %v\n",
			v.A,
		)
	}
}

func TestReadJSONBodyError(t *testing.T) {
	v := V{}
	req, err := http.NewRequest(
		"GET",
		"localhost/index.html",
		errorReader{},
	)
	if err != nil {
		t.Fatalf("ReadJSONBody: http.NewRequest failed: '%v'\n", err)
	}
	err = ReadJSONBody(req, &v)
	if err == nil {
		t.Errorf("ReadJSONBody returned no error: '%v'\n", err)
	}
}
