package util

import (
	log "github.com/sirupsen/logrus"
)

func FatalError(err error) {
	if err != nil {
		log.Panic(err)
	}
}
