package util

import (
	"testing"
)

func TestFileExists(t *testing.T) {
	if !FileExists(".") {
		t.Error("'.' does not exist")
	}
}
