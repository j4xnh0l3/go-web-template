package main

import (
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/jaelen/go-web-template/cli"
	"gitlab.com/jaelen/go-web-template/config"
	"gitlab.com/jaelen/go-web-template/mongo"
	"gitlab.com/jaelen/go-web-template/routes"
)

// globals

var HttpServer *http.Server

func main() {
	var err error

	log.Println("Parsing and validating CLI arguments")
	cli.Parse()
	cli.Validate()

	log.Printf(
		"Loading and validating config '%s'\n",
		*cli.Config,
	)
	cfg := config.LoadConfig(*cli.Config)
	config.Cfg = &cfg
	config.Cfg.Validate()
	log.Println("Setting up routes")
	routes.SetupRoutes()
	log.Println(
		"Ensuring MongoDB indices (if this hangs,",
		"Mongo is probably unreachable)",
	)
	mongo.EnsureIndices()
	log.Println("Starting, listening on", config.Cfg.ServeAddress)
	HttpServer = &http.Server{
		Addr: config.Cfg.ServeAddress,
	}
	err = HttpServer.ListenAndServe()
	log.Println(err)
	log.Println("Terminating")
}
