package routes

import (
	"testing"

	"gitlab.com/jaelen/go-web-template/config"
)

func TestSetupRoutes(t *testing.T) {
	config.Cfg = &config.Config{
		StaticDir: ".",
	}
	SetupRoutes()
}
