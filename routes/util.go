package routes

import (
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/jaelen/go-web-template/config"
	"gitlab.com/jaelen/go-web-template/mongo"
	"gitlab.com/jaelen/go-web-template/prom"
	"gitlab.com/jaelen/go-web-template/util"
)

type Validator interface {
	Validate() bool
}

func logRequest(r *http.Request) {
	log.Println(
		"Received",
		r.Method,
		"to",
		r.RequestURI,
		"from",
		r.RemoteAddr,
	)
}

func badRequestInvalidMethod(
	w http.ResponseWriter,
	r *http.Request,
) {
	log.Println(
		"400 Bad Request, invalid method",
		r.Method,
	)
	http.Error(
		w,
		"400 Bad Request: Invalid method",
		400,
	)
}

func ValidateAndInsert(
	w http.ResponseWriter,
	r *http.Request,
	v Validator,
	collection string,
) error {
	err := util.ReadJSONBody(r, v)
	if err != nil || !v.Validate() {
		w.WriteHeader(http.StatusBadRequest)
		prom.FooFailureTotal.Inc()
		return err
	}
	session := mongo.Session()
	defer session.Close()
	db := session.DB(config.Cfg.Database)
	c := db.C(collection)
	err = c.Insert(v)
	util.FatalError(err)
	w.WriteHeader(http.StatusOK)
	return nil
}
