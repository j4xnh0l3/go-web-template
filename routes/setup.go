package routes

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/jaelen/go-web-template/config"
)

/* Aggregate function to create the routes defined in the other files
 */
func SetupRoutes() {
	http.HandleFunc("/example", ExampleRoute)
	http.Handle("/metrics", promhttp.Handler())

	// static content, delete or comment this out if not needed
	if config.Cfg.StaticDir != "" {
		http.Handle(
			"/",
			http.StripPrefix(
				"/",
				http.FileServer(
					http.Dir(config.Cfg.StaticDir),
				),
			),
		)
	}
}
