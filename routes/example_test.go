package routes

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	log "github.com/sirupsen/logrus"
	"gitlab.com/jaelen/go-web-template/config"
)

func TestExampleRouteGet(t *testing.T) {
	exampleGet, _ := http.NewRequest(
		"GET",
		"/example",
		nil,
	)

	log.Println(
		"TestExampleRoute: Method: GET",
		"URI:", exampleGet.URL.Path,
	)
	w := httptest.NewRecorder()
	ExampleRoute(w, exampleGet)
}

func TestExampleRoutePost(t *testing.T) {
	cfg := config.LoadConfig("../test_data/test_config.json")
	config.Cfg = &cfg
	examplePost, _ := http.NewRequest(
		"POST",
		"/example",
		bytes.NewBuffer(
			[]byte(`{"Foo": "a", "Bar": true, "Baz": 123}`),
		),
	)

	log.Println(
		"TestExampleRoute: Method: POST",
		"URI:", examplePost.URL.Path,
	)
	w := httptest.NewRecorder()
	ExampleRoute(w, examplePost)
}

// invalid method, should 404
func TestExampleRoutePatch(t *testing.T) {
	examplePatch, _ := http.NewRequest(
		"PATCH",
		"/example",
		bytes.NewBuffer([]byte(`{"some_key": "some_value"}`)),
	)
	log.Println(
		"TestExampleRoute: Method: PATCH",
		"URI:", examplePatch.URL.Path,
	)
	w := httptest.NewRecorder()
	ExampleRoute(w, examplePatch)
}
