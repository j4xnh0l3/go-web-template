# go-web-template

A Golang skeleton project for a RESTful API service backed by MongoDB.
You should replace the text in this README with something that
documents TF out of your service.

# Features
- 100% unit test coverage out of the box
- Handy forking script to make it your own
- MongoDB back-end
- Prometheus /metrics URI
- Structured logging with Logrus

# Development Requirements
- A recent version of Golang
- `export GO111MODULE=on` if `$GOPATH` is set and `pwd` is inside of `$GOPATH`
- A local installation of MongoDB-server

# Unit Tests
Run the unit tests with `make test` instead of `go test`, as the unit tests
rely on spawning a local mongod using a subdirectory of /tmp instead of mocking
out MongoDB.

# Releasing
Update the version information in `meta.json`.  If using a Red Hat OS, you
can build an RPM package with `make rpm`.

# Running in Production
The application can be configured with environment variables defined in
`conifg/config.go`, which can override values in the config file (if any
config file is used).  Additionally, there is a `CONFIG_PATH` environment
variable that can override the default config path (which in turn, is
overridden by using the `-config` command line option).

The Docker image is very minimal, you may wish to change the base image to
something that includes additional tools, certificates, etc...

