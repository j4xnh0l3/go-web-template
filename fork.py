#!/usr/bin/env python3

import glob
import os
import sys


def _(x):
    print(x)
    retcode = os.system(x)
    assert not retcode, retcode


def replace(path, found, replaced):
    with open(path) as f:
        text = f.read()
    text = text.replace(found, replaced)
    with open(path, 'w') as f:
        f.write(text)


def main():
    file_path = os.path.abspath(__file__)
    dirname = os.path.dirname(file_path)
    os.chdir(dirname)

    name = input("Enter the new project name: ").strip()
    assert name
    assert name.islower(), name
    assert name.replace("_", "").isalpha(), name
    assert len(name) < 20, len(name)

    import_url = input(
        "Enter the import URL, ie:  gitlab.com/jaelen/newproject"
    )
    assert import_url
    assert " " not in import_url, import_url
    assert import_url.count('/') == 2, import_url
    assert import_url.islower(), import_url


    paths = glob.glob('*.go') + \
        glob.glob('*/*.go') + \
        glob.glob('files/*') + [
        '.gitignore',
        'Makefile',
        'myexe.spec',
    ]
    for path in paths:
        replace(
            path,
            'myexe',
            name,
        )
        replace(
            path,
            'gitlab.com/jaelen/go-web-template',
            import_url,
        )
    _('mv myexe.spec {}.spec'.format(name))
    _('rm -rf .git')
    _('git init .')
    _('git add .')
    _('git commit -am "Initial commit"')

    print("Successfully forked.")
    print(
        "Add a new remote origin with "
        "`git remote add origin $GIT_CLONE_URL`"
    )


if __name__ == "__main__":
    main()
